<?php
class Operation{
	
	private $numOperation;
	private $compte;
	private $libelle;
	private $montant;
	private $nature;
	private $dateOp;
	
	public function __construct(){
		
	}
	
	public function getNumOperation(){
		return $this->numOperation;
	}
	public function getCompte(){
		return $this->compte;
	}
	public function getMontant(){
		return $this->montant;
	}
	public function getNature(){
		return $this->nature;
	}
	public function getDateOp(){
		return $this->dateOp;
	}
	public function getLibelle(){
		return $this->libelle;
	}
	
	public function setNumOperation($n){
		$this->numOperation = $n;
	}
	public function setCompte($c){
		$this->compte = $c;
	}
	public function setMontant($m){
		$this->montant = $m;
	}
	public function setNature($n){
		$this->nature = $n;
	}
	public function setDateOp($d){
		$this->dateOp = $d;
	}
	public function setLibelle($l){
		$this->libelle = $l;
	}
}

?>