<?php
class Compte{
	
	private $numCompte;
	private $titulaire;
	private $ouverture;
	private $montant;
	private $lesOperations;
	
	public function __construct(){
		$lesOperations = array();
	}
	
	public function setNumCompte($n){
		$this->numCompte = $n;
	}	
	public function setTitulaire($t){
		$this->titulaire = $t;
	}	
	public function setouverture($o){
		$this->ouverture = $o;
	}	
	public function setMontant($m){
		$this->montant = $m;
	}
	public function getMontant(){
		return $this->montant;
	}
	public function getNumCompte(){
		return $this->numCompte;
	}
	public function getTitulaire(){
		return $this->titulaire;
	}
	public function getOuverture(){
		return $this->ouverture;
	}
	
	public function ajouterOperation($ope){
		$ope->setCompte($this->getNumCompte());
		$this->lesOperations[] = $ope;
	}
	
	public function getLesOperations(){
		return $this->lesOperations;
	}
}
?>