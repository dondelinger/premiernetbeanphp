<?php
require_once('Compte.php');
require_once('Operation.php');
require_once './PDOComptes.php';

$bdd = new PDOComptes();

if(isset($_POST['creer_c'])){
	// instanciation du compte et hydratation de celui-ci (SETTERS)
	$c1 = new Compte();
	$c1->setNumCompte($_POST['numero_c']);
	$c1->setTitulaire($_POST['titulaire']);
	$c1->setMontant($_POST['montant_c']);
        
        $bdd->insertCompte($c1);
/*
	$o = new Operation();
	$o->setNumOperation("1");
	$o->setNature("C");
	$o->setLibelle("Salaire");
	$o->setMontant(2000);

	$c1->ajouterOperation($o);
*/
}

$lesComptes = $bdd->findAllComptes();
?>

<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Gestion des comptes</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
  <h1>Gestion des comptes banque SIO</h1>
  
  <form method="POST" action="">
      <fieldset>
        <legend>Creation d'un compte</legend>
        <div class="form-group">
                    <label for="taille_1">Numéro du compte</label>
                    <input type="text" name="numero_c" id="numero_c" class="form-control" value="" />
        </div>
        <div class="form-group">
		<label for="titulaire">Titulaire</label>
		<input type="text" name="titulaire" id="titulaire" class="form-control" value="" />
        </div>
        <div class="form-group">
		<label for="titulaire">Montant initial</label>
		<input type="text" name="montant_c" id="montant_c" class="form-control" value="" />
        </div>
      <input type="submit" name="creer_c" value="Créer" class="btn btn-default"/>
    </fieldset>
  </form>
  
      <div class="table-responsive">
      <table class='table'>
          <tr>
              <th>NUMERO</th>
              <th>TITULAIRE</th>
              <th>SOLDE</th>
          </tr>
          
          <?php
          foreach ($lesComptes as $c) {
              echo "<tr><td>".$c->getNumCompte()."</td><td>".$c->getTitulaire()."</td><td>".$c->getMontant()."</td></tr>";
          }
          ?>
      </table>
      </div>
</div>
</body>
</html>